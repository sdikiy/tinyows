/*
 * alist
 */

/**
 * Initialize an alist structure
 */
alist *alist_init();

/**
 * Free an alist structure
 */
void alist_free(alist * al);

/**
 * Add a given buffer to the end of an alist entry
 * if key exist, value is add to the list
 * if not new key entry is created
 */
void alist_add(alist * al, buffer * key, buffer * value);

/**
 * Check if a given key string is or not in the alist
 */
bool alist_is_key(const alist * al, const char *key);

/**
 * Return a value buffer from an alist (from key value)
 * You must be sure key is defined for this array, see is_key() above
 * Carreful return a reference on the alist buf itself !
 */
list *alist_get(const alist * al, const char *key);

/**
 * Flush an alist to a given file
 * (mainly to debug purpose)
 */
void alist_flush(const alist * al, FILE * output);


/*
 * array
 */

/**
 * Initialize an array structure
 */
array *array_init();

/**
 * Free an array structure
 */
void array_free(array * a);

/**
 * Add a given buffer to the end of an array
 * Carefull key and value are passed by reference
 * and will be released by array_free !
 */
void array_add(array * a, buffer * key, buffer * value);

/**
 * Check if a given key string is or not in the array
 */
bool array_is_key(const array * a, const char *key);

/**
 * Return a value buffer from an array (from key value)
 * You must be sure key is defined for this array, see is_key() above
 * Carreful return a reference on the array buf itself !
 */
buffer *array_get(const array * a, const char *key);

/**
 * Flush an array to a given file
 * (mainly to debug purpose)
 */
void array_flush(const array * a, FILE * output);


/*
 * buffer
 */

/**
 * Realloc buffer memory space
 * use an exponential grow size to realloc (less realloc call)
 */
void buffer_realloc(buffer * buf);

/**
 * Initialize buffer structure
 */
buffer *buffer_init();

/**
 * Free a buffer structure and all data in it
 */
void buffer_free(buffer * buf);

/**
 * Empty data from a given buffer
 * (don't release memory, use buffer_free instead)
 */
void buffer_empty(buffer * buf);

/**
 * Flush all data from a buffer to a stream
 * Not used *only* to debug purpose ;)
 */
void buffer_flush(buffer * buf, FILE * output);

/**
 * Add a char to the bottom of a buffer
 */
void buffer_add(buffer * buf, char c);

/**
 * Return a buffer from a double
 */
buffer *buffer_ftoa(double f);

/**
 * Add a double to a given buffer
 */
void buffer_add_double(buffer * buf, double f);

/**
 * Add an int to a given buffer
 */
void buffer_add_int(buffer * buf, int i);

/**
 * Convert an integer to a buffer (base 10 only)
 */
buffer *buffer_itoa(int i);

/**
 * Convert a string to a buffer
 */
buffer *buffer_from_str(const char *str);

/**
 * Add a char to the top of a buffer
 */
void buffer_add_head(buffer * buf, char c);

/**
 * Add a string to the top of a buffer
 */
void buffer_add_head_str(buffer * buf, char *str);

/**
 * Add a string to a buffer
 */
void buffer_add_str(buffer * buf, const char *str);

/**
 * Check if a buffer string is the same than another
 */
bool buffer_cmp(const buffer * buf, const char *str);

/**
 * Check if a buffer string is the same than another, on the n first char
 */
bool buffer_ncmp(const buffer * buf, const char *str, size_t n);

/**
 * Check if a buffer string is the same than anoter for a specified length
 * (insensitive case check)
 */
bool buffer_case_cmp(const buffer * buf, const char *str);

/**
 * Copy data from a buffer to an another
 */
void buffer_copy(buffer * dest, const buffer * src);

/**
 * Delete last N chars from a buffer
 */
void buffer_pop(buffer * buf, size_t len);

/**
 * Delete first N chars from a buffer
 */
void buffer_shift(buffer * buf, size_t len);

/**
 * Replace all occurences of string 'before' inside the buffer by string 'after'
 */
buffer *buffer_replace(buffer * buf, char *before, char *after);

/**
 * Modify string to replace encoded characters by their true value
 * Function originaly written by Assefa
 *
 * The replacements performed are:
 * \code
 *  &	-> &amp;
 *  "	-> &quot;
 *  <	-> &lt;
 *  >	-> &gt;
 * \endcode
 */
buffer *buffer_encode_xml_entities_str(const char * str);

/**
 * Modify string to replace encoded characters by their true value
 * for JSON output
 *
 * The replacements performed are:\n
 *  " -> \"
 */
buffer *buffer_encode_json_str(const char * str);


/*
 * list
 */

/**
 * Initialize a list structure
 */
list *list_init();

/**
 * Free a list structure
 */
void list_free(list * l);

/**
 * Add a given buffer to the end of a list
 * Careful buffer is passed by reference,
 * and must be free with list_free()
 */
void list_add(list * l, buffer * value);

/**
 * Add a given buffer to the end of a list
 * Careful buffer is passed by reference,
 * and must be free with list_free()
 */
void list_add_str(list * l, char *value);

/**
 * Add a given list to the end of a list
 * Careful list is passed by reference,
 * and must be free with list_free()
 */
void list_add_list(list * l, list * l_to_add);

/**
 * Add a given buffer by copy to the end of a list
 * Careful buffer is passed by reference,
 * and must be free with list_free()
 */
void list_add_by_copy(list * l, buffer * value);

/**
 * Initialize a list node
 */
list_node *list_node_init();

/**
 * Free a list node
 */
void list_node_free(list * l, list_node * ln);

/**
 * Check if a given buffer value is or not in the list
 */
bool in_list(const list * l, const buffer * value);

/**
 * Check if a given string value is or not in the list
 */
bool in_list_str(const list * l, const char * value);

/**
 * Trunk an initial buffer into several pieces upon a separator char
 * Careful returned list must then be free with list_free()
 */
list *list_explode(char separator, const buffer * value);

/**
 * Trunk an initial buffer into several pieces upon two separators
 * Careful returned list must then be free with list_free()
 */
list *list_explode_start_end(char separator_start, char separator_end, buffer * value);

/**
 * Trunk an initial string into several pieces upon a separator char
 * Careful returned list must then be free with list_free()
 */
list *list_explode_str(char separator, const char *value);

/**
 * Trunk an initial string into several pieces upon a separator char
 * Careful returned list must then be free with list_free()
 */
list *list_explode_str_trim(char separator, const char *value);

/**
 * Flush a list to a given file
 * (mainly to debug purpose)
 */
void list_flush(const list * l, FILE * output);


/*
 * multiple list
 */

/**
 * Initialize a multiple list structure
 */
mlist *mlist_init();

/**
 * Free a multiple list structure
 */
void mlist_free(mlist * ml);

/**
 * Add a given list to the end of a mlist
 * Careful list is passed by reference,
 * and must be free with mlist_free()
 */
void mlist_add(mlist * ml, list * value);

/**
 * Initialize a multiple list node
 */
mlist_node *mlist_node_init();

/**
 * Free a multiple list node
 */
void mlist_node_free(mlist * ml, mlist_node * mln);

/**
 * Trunk an initial buffer into several pieces upon two separators
 * Inside multiple list, each element is separated by a comma
 * Careful returned multiple list must then be free with mlist_free()
 */
mlist *mlist_explode(char separator_start, char separator_end, buffer * value);

/**
 * Flush a mlist to a given file
 * (mainly to debug purpose)
 */
void mlist_flush(const mlist * ml, FILE * output);
