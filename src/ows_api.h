/*
  Copyright (c) <2007-2012> <Barbara Philippot - Olivier Courtin>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
  copies of the Software, and to permit persons to whom the Software is 
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
  IN THE SOFTWARE.
*/

#include "struct/struct.h"

buffer *cgi_add_xml_into_buffer (buffer * element, xmlNodePtr n);
char *cgi_getback_query (ows * o);
bool cgi_method_get ();
bool cgi_method_post ();
array *cgi_parse_kvp (ows * o, char *query);
array *cgi_parse_xml (ows * o, char *query);
bool check_regexp (const char *str_request, const char *str_regex);
buffer *fe_comparison_op (ows * o, buffer * typename, filter_encoding * fe, xmlNodePtr n);
buffer *fe_envelope (ows * o, buffer * typename, filter_encoding * fe, buffer *envelope, xmlNodePtr n);
void fe_error (ows * o, filter_encoding * fe);
buffer *fe_expression (ows * o, buffer * typename, filter_encoding * fe, buffer * sql, xmlNodePtr n);
buffer *fe_feature_id (ows * o, buffer * typename, filter_encoding * fe, xmlNodePtr n);
filter_encoding *fe_filter (ows * o, filter_encoding * fe, buffer * typename, buffer * xmlchar);
void fe_filter_capabilities_100 (const ows * o);
void fe_filter_capabilities_110 (const ows * o);
buffer *fe_function (ows * o, buffer * typename, filter_encoding * fe, buffer * sql, xmlNodePtr n);
bool fe_is_comparison_op (char *name);
bool fe_is_logical_op (char *name);
bool fe_is_spatial_op (char *name);
buffer *fe_kvp_bbox (ows * o, wfs_request * wr, buffer * layer_name, ows_bbox * bbox);
buffer *fe_kvp_featureid (ows * o, wfs_request * wr, buffer * layer_name, list * fid);
buffer *fe_logical_op (ows * o, buffer * typename, filter_encoding * fe, xmlNodePtr n);
void fe_node_flush (xmlNodePtr node, FILE * output);
buffer *fe_property_name (ows * o, buffer * typename, filter_encoding * fe, buffer * sql, xmlNodePtr n, bool check_geom_column, bool mandatory);
buffer *fe_spatial_op (ows * o, buffer * typename, filter_encoding * fe, xmlNodePtr n);
buffer *fe_xpath_property_name (ows * o, buffer * typename, buffer * property);
buffer *fill_fe_error (ows * o, filter_encoding * fe);
void filter_encoding_flush (filter_encoding * fe, FILE * output);
void filter_encoding_free (filter_encoding * fe);
filter_encoding *filter_encoding_init ();

int main (int argc, char *argv[]);

ows_bbox *ows_bbox_boundaries (ows * o, list * from, list * where, ows_srs * srs);
void ows_bbox_flush (const ows_bbox * b, FILE * output);
void ows_bbox_free (ows_bbox * b);
ows_bbox *ows_bbox_init ();
bool ows_bbox_set (ows * o, ows_bbox * b, double xmin, double ymin, double xmax, double ymax, int srid);
bool ows_bbox_set_from_geobbox (ows * o, ows_bbox * bb, ows_geobbox * geo);
bool ows_bbox_set_from_str (ows * o, ows_bbox * bb, const char *str, int srid);
bool ows_bbox_transform (ows * o, ows_bbox * bb, int srid);
void ows_bbox_to_query(ows * o, ows_bbox *bbox, buffer *query);
void ows_contact_flush (ows_contact * contact, FILE * output);
void ows_contact_free (ows_contact * contact);
ows_contact *ows_contact_init ();
void ows_error (ows * o, enum ows_error_code code, char *message, char *locator);

#include "ows/ows_db.h"
#include "ows/ows_mysql.h"
#include "ows/ows_psql.h"
#include "ows/ows_layer.h"

void ows_flush (ows * o, FILE * output);
void ows_free (ows * o);
ows_geobbox *ows_geobbox_compute (ows * o, buffer * layer_name);
void ows_geobbox_flush (const ows_geobbox * g, FILE * output);
void ows_geobbox_free (ows_geobbox * g);
ows_geobbox *ows_geobbox_init ();
ows_geobbox *ows_geobbox_copy(ows_geobbox *g);
bool ows_geobbox_set (ows * o, ows_geobbox * g, double west, double east, double south, double north);
bool ows_geobbox_set_from_bbox (ows * o, ows_geobbox * g, ows_bbox * bb);
ows_geobbox *ows_geobbox_set_from_str (ows * o, ows_geobbox * g, char *str);
void ows_get_capabilities_dcpt (const ows * o, const char * req);

void ows_metadata_fill (ows * o, array * cgi);
void ows_metadata_flush (ows_meta * metadata, FILE * output);
void ows_metadata_free (ows_meta * metadata);
ows_meta *ows_metadata_init ();

void ows_parse_config(ows * o, const char *filename);

void ows_request_check (ows * o, ows_request * or, const array * cgi, const char *query);
void ows_request_flush (ows_request * or, FILE * output);
void ows_request_free (ows_request * or);
ows_request *ows_request_init ();
int ows_schema_validation (ows * o, buffer * xml_schema, buffer * xml, bool schema_is_file, enum ows_schema_type schema_type);
void ows_service_identification (const ows * o);
void ows_service_metadata (const ows * o);
void ows_service_provider (const ows * o);
void ows_srs_flush (ows_srs * c, FILE * output);
void ows_srs_free (ows_srs * c);
buffer *ows_srs_get_from_a_srid (ows * o, int srid);
list *ows_srs_get_from_srid (ows * o, list * l);
int ows_srs_get_srid_from_layer (ows * o, buffer * layer_name);
ows_srs *ows_srs_init ();
bool ows_srs_meter_units (ows * o, buffer * layer_name);
ows_srs *ows_srs_copy(ows_srs * d, ows_srs * s);
bool ows_srs_set_geobbox(ows * o, ows_srs * s);
bool ows_srs_set (ows * o, ows_srs * c, const buffer * auth_name, int auth_srid);
bool ows_srs_set_from_srid (ows * o, ows_srs * s, int srid);
bool ows_srs_set_from_srsname(ows * o, ows_srs * s, const char *srsname);
void ows_usage (ows * o);
void ows_version_flush (ows_version * v, FILE * output);
void ows_version_free (ows_version * v);
bool ows_version_check(ows_version *v);
bool ows_version_set_str(ows_version * v, char *str);
int ows_version_get (ows_version * v);
ows_version *ows_version_init ();
void ows_version_set (ows_version * v, int major, int minor, int release);
void wfs (ows * o, wfs_request * wf);
void wfs_delete (ows * o, wfs_request * wr);
void wfs_describe_feature_type (ows * o, wfs_request * wr);
buffer * wfs_generate_schema(ows * o, ows_version * version);
void wfs_error (ows * o, wfs_request * wf, enum wfs_error_code code, char *message, char *locator);
void wfs_get_capabilities (ows * o, wfs_request * wr);
void wfs_get_feature (ows * o, wfs_request * wr);
void wfs_gml_feature_member (ows * o, wfs_request * wr, buffer * layer_name, list * properties, PGresult * res);
void wfs_parse_operation (ows * o, wfs_request * wr, buffer * op);
void wfs_request_check (ows * o, wfs_request * wr, const array * cgi);
void wfs_request_flush (wfs_request * wr, FILE * output);
void wfs_request_free (wfs_request * wr);
wfs_request *wfs_request_init ();
buffer *wfs_request_remove_namespaces (ows * o, buffer * b);

ows_layer_storage * ows_layer_storage_init();
void ows_layer_storage_free(ows_layer_storage * storage);
void ows_layer_storage_flush(ows_layer_storage * storage, FILE * output);

void ows_layers_storage_fill(ows * o);
void ows_layers_storagefill(ows * o);

void ows_layers_storage_flush(ows * o, FILE * output);
void ows_log(ows *o, int log_level, const char *log);
void ows_parse_config_mapfile(ows *o, const char *filename);
bool ows_libxml_check_namespace(ows *o, xmlNodePtr n);
