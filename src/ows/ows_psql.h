/**
 * Connect the ows to the PG database
 */
void ows_db_conn_pg(ows * o, ows_db * db);

/**
 * \brief
 * Execute an SQL request 
 * \deprecated
 * use ows_psql_exe()
 */
PGresult * ows_psql_exec(ows *o, const char *sql);

/**
 * Execute an SQL request 
 */
PGresult * ows_psql_exe(ows * o, ows_db * db, const char * sql);

/**
 * Check if a given WKT geometry is or not valid
 */
bool ows_psql_is_geometry_valid(ows * o, buffer * geom);

/**
 * Return the column's name matching the specified number from table
 * (Only use in specific FE position function, so not directly inside
 *  storage handle mechanism)
 */
buffer *ows_psql_column_name(ows * o, buffer * layer_name, int number);

/**
 * Retrieve an ows_version, related to current PostGIS version.
 */
ows_version * ows_psql_postgis_version(ows *o);

/**
 * Convert a PostgreSql type to a valid
 * OGC XMLSchema's type
 */
char *ows_psql_to_xsd(buffer * type, ows_version *version);

/**
 * Convert a date from PostgreSQL to XML
 */
buffer *ows_psql_timestamp_to_xml_time(char *timestamp);

/**
 * Generate a new buffer id supposed to be unique for a given layer name
 */
buffer *ows_psql_generate_id(ows * o, buffer * layer_name);

/**
 * Return the number of rows returned by the specified requests
 */
int ows_psql_number_features(ows * o, list * from, list * where);

/**
 * Parse...
 */
xmlNodePtr ows_psql_recursive_parse_gml(ows * o, xmlNodePtr n, xmlNodePtr result);

/**
 * Transform a GML geometry to PostGIS EWKT
 * Return NULL on error
 */
buffer * ows_psql_gml_to_sql(ows * o, xmlNodePtr n, int srid);

/**
 * Use PostgreSQL native handling to escape string
 * string returned must be freed by the caller
 * return NULL on error
 */
char *ows_psql_escape_string(ows *o, const char *content);

/**
 * Return SRID from a given geometry
 */
int ows_psql_geometry_srid(ows *o, const char *geom);

/**
 * Fill layer storage from given db
 */
void ows_psql_layer_storage_fill(ows * o, ows_layer * l);

/**
 * Retrieve columns name and type of a table related a given layer
 */
void ows_psql_storage_fill_attributes(ows * o, ows_layer * l);

/**
 * Retrieve not_null columns of a table related a given layer
 */
void ows_psql_storage_fill_not_null(ows * o, ows_layer * l);

/**
 * Retrieve pkey column of a table related a given layer
 * And if success try also to retrieve a related pkey sequence
 */
void ows_psql_storage_fill_pkey(ows * o, ows_layer * l);

/**
 * Retrieve columns character_maximum_length of a table related a given layer,
 * and fill layer->storage->char_max_length
 */
void ows_psql_storage_fill_char_max_length(ows * o, ows_layer * l);

/**
 * Fill the list of possible values for a column according to the constraint value.
 * (layer->storage->constraints)
 * Used to return enumeration constraints in describe feature type request.
 */
void ows_psql_storage_fill_constraints(ows * o, ows_layer * l);
