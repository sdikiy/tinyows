/**
 * Initialize an aws layer structure
 */
ows_layer_list *ows_layer_list_init();

/**
 * Free an ows layer structure
 */
void ows_layer_list_free(ows_layer_list * ll);

/**
 * Retrieve a Layer from a layer list or NULL if not found
 */
ows_layer * ows_layer_get(const ows_layer_list * ll, const buffer * name);

/**
 * Check if a layer matchs an existing table in PostGIS
 */
bool ows_layer_match_table(const ows * o, const buffer * name);

/**
 * Retrieve a list of layer name who have a storage object
 * (concrete layer so)
 */
list * ows_layer_list_having_storage(const ows_layer_list * ll);

/**
 * Check if all layers are retrievable (defined in configuration file)
 */
bool ows_layer_list_retrievable(const ows_layer_list * ll);

/**
 * Check if a layer is retrievable (defined in configuration file)
 */
bool ows_layer_retrievable(const ows_layer_list * ll, const buffer * name);

/**
 * Check if all layers are writable (defined in file conf)
 */
bool ows_layer_list_writable(const ows_layer_list * ll);

/**
 * Check if a layer is writable (defined in file conf)
 */
bool ows_layer_writable(const ows_layer_list * ll, const buffer * name);

/**
 * Check if a layer is in a layer's list
 */
bool ows_layer_in_list(const ows_layer_list * ll, buffer * name);

/**
 * Check if a layer's list is in a other layer's list
 */
bool ows_layer_list_in_list(const ows_layer_list * ll, const list * l);

/**
 * Return an array of prefixes and associated NS URI of a layer's list
 */
array *ows_layer_list_namespaces(ows_layer_list * ll);

/**
 * Return a list of layer names grouped by prefix
 */
list *ows_layer_list_by_ns_prefix(ows_layer_list * ll, list * layer_name, buffer * ns_prefix);

/**
 * Retrieve a list of prefix used for a specified list of layers
 */
list *ows_layer_list_ns_prefix(ows_layer_list * ll, list * layer_name);

/**
 * Retrieve the prefix linked to the specified layer
 */
buffer *ows_layer_ns_prefix(ows_layer_list * ll, buffer * layer_name);

/**
 * Retrieve the ns_uri associated to the specified ns_prefix
 */
buffer *ows_layer_ns_uri(ows_layer_list * ll, buffer * ns_prefix);

/**
 * Add a layer to a layer's list
 */
void ows_layer_list_add(ows_layer_list * ll, ows_layer * l);

/**
 * Initialize a layer node
 */
ows_layer_node *ows_layer_node_init();

/**
 * Free a layer node
 */
void ows_layer_node_free(ows_layer_list * ll, ows_layer_node * ln);

/**
 * Flush a layer's list to a given file
 * (mainly to debug purpose)
 */
void ows_layer_list_flush(ows_layer_list * ll, FILE * output);

/**
 * Initialize a layer
 */
ows_layer *ows_layer_init();

/**
 * Free a layer
 */
void ows_layer_free(ows_layer * l);

/**
 * Flush a layer to a given file
 * (mainly to debug purpose)
 */
void ows_layer_flush(ows_layer * l, FILE * output);

/**
 * Return the name of the id column from table matching layer name
 */
buffer *ows_layer_id_column(ows * o, buffer * layer_name);

/**
 * Return the column number of the id column from table matching layer name
 * (needed in wfs_get_feature only)
 */
int ows_layer_column_number_id_column(ows * o, buffer * layer_name);

/**
 * Return geometry columns from the table matching layer name
 */
list *ows_layer_geometry_column(ows * o, buffer * layer_name);

/**
 * Return schema name from a given layer 
 */
buffer *ows_layer_schema_name(ows * o, buffer * layer_name);

/**
 * Return table name from a given layer 
 */
buffer *ows_layer_table_name(ows * o, buffer * layer_name);

/**
 * Check if the specified column from a layer_name is (or not) a geometry column
 */
bool ows_layer_is_geometry_column(ows * o, buffer * layer_name, buffer * column);

/**
 * Return a list of not null properties from the table matching layer name
 */
list *ows_layer_not_null_properties(ows * o, buffer * layer_name);

/**
 * Retrieve description of a table matching a given layer name
 */
array *ows_layer_describe_table(ows * o, buffer * layer_name);

/**
 * Return the type of the property passed in parameter
 */
buffer *ows_layer_type(ows * o, buffer * layer_name, buffer * property);
