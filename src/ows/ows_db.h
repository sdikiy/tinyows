/**
 * Initialize a db
 */
ows_db *ows_db_init();

/**
 * Free a layer
 */
void ows_db_free(ows_db * db);

/**
 * Initialize a db node
 */
ows_db_node *ows_db_node_init();

/**
 * Free a db node
 */
void ows_db_node_free(ows_db_list * ll, ows_db_node * ln);

/**
 * Initialize an ows db structure
 */
ows_db_list *ows_db_list_init();

/**
 * Free an ows db structure
 */
void ows_db_list_free(ows_db_list * db_list);

/**
 * Add a db to a db's list
 */
void ows_db_list_add(ows_db_list * db_list, ows_db * db);

/**
 * Retrieve a db from a db list or NULL if not found
 */
ows_db * ows_db_get_by_name(const ows_db_list * db_list, const buffer * name);

/**
 * Connect the ows to databases specified in configuration file
 */
void ows_db_conn(ows * o);

/**
 * Flush a db to a given file
 * (mainly to debug purpose)
 */
void ows_db_flush(ows_db * db, FILE * output);

/**
 * Flush a db's list to a given file
 * (mainly to debug purpose)
 */
void ows_db_list_flush(ows_db_list * db_list, FILE * output);
