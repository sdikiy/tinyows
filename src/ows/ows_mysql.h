/**
 * Connect the ows to the MySQL database
 */
void ows_db_conn_mysql(ows * o, ows_db * db);

/**
 * Fill layer storage from given db
 */
void ows_mysql_layer_storage_fill(ows * o, ows_layer * layer);
