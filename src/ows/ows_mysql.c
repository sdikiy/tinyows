#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <string.h>

#include "ows.h"


/*
 * Connect the ows to the MySQL database
 */
void ows_db_conn_mysql(ows * o, ows_db * db)
{
    assert(o);
    assert(db);

    /*
    mysql_real_connect(db->mysql, bla, bla, bla...);
    */
    db->mysql = mysql_init(NULL);
    /* Connect to database */
    if (!mysql_real_connect(
            db->mysql,
            array_get(db->db_dsn, (char *)"host")->buf,
            array_get(db->db_dsn, (char *)"user")->buf,
            array_get(db->db_dsn, (char *)"password")->buf,
            array_get(db->db_dsn, (char *)"dbname")->buf,
            0, NULL, 0)) {
        buffer_add_str(db->type, "\n    NO conn - ");
        buffer_add_str(db->type, mysql_error(db->mysql));
    }

}


/*
 * Fill layer storage from given db
 */
void ows_mysql_layer_storage_fill(ows * o, ows_layer * layer)
{

}

