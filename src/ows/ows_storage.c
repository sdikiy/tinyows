/**
\file ows_starage.c

\brief ...
\details ...

\author Barbara Philippot
\author Olivier Courtin

\date 2007-2012

\copyright
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
\copyright
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
\copyright
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "ows.h"


ows_layer_storage * ows_layer_storage_init()
{
    ows_layer_storage * storage;

    storage = malloc(sizeof(ows_layer_storage));
    assert(storage);

    /* default values:  srid='-1' */
    storage->db_name = buffer_init();
    storage->schema = buffer_init();
    storage->table = buffer_init();
    storage->db = NULL;
    storage->srid = -1;
    storage->geom_columns = list_init();
    storage->is_degree = true;
    storage->pkey = NULL;
    storage->pkey_sequence = NULL;
    storage->pkey_default = NULL;
    storage->pkey_column_number = -1;
    storage->attributes = array_init();
    storage->char_max_length = array_init();
    storage->constraints = alist_init();
    storage->not_null_columns = NULL;

    return storage;
}


void ows_layer_storage_free(ows_layer_storage * storage)
{
    assert(storage);

    if (storage->db_name)          buffer_free(storage->db_name);
    if (storage->schema)           buffer_free(storage->schema);
    if (storage->table)            buffer_free(storage->table);
    if (storage->pkey)             buffer_free(storage->pkey);
    if (storage->pkey_sequence)    buffer_free(storage->pkey_sequence);
    if (storage->pkey_default)     buffer_free(storage->pkey_default);
    if (storage->geom_columns)     list_free(storage->geom_columns);
    if (storage->attributes)       array_free(storage->attributes);
    if (storage->char_max_length)  array_free(storage->char_max_length);
    if (storage->constraints)      alist_free(storage->constraints);
    if (storage->not_null_columns) list_free(storage->not_null_columns);

    free(storage);
    storage = NULL;
}


#ifdef OWS_DEBUG
void ows_layer_storage_flush(ows_layer_storage * storage, FILE * output)
{
    assert(storage);
    assert(output);

    if (storage->db_name) {
        fprintf(output, "db_name: ");
        buffer_flush(storage->db_name, output);
        fprintf(output, "\n");
    }

    if (storage->schema) {
        fprintf(output, "schema: ");
        buffer_flush(storage->schema, output);
        fprintf(output, "\n");
    }

    if (storage->table) {
        fprintf(output, "table: ");
        buffer_flush(storage->table, output);
        fprintf(output, "\n");
    }

    if (storage->geom_columns) {
        fprintf(output, "geom_columns: ");
        list_flush(storage->geom_columns, output);
        fprintf(output, "\n");
    }

    fprintf(output, "srid: %i\n", storage->srid);
    fprintf(output, "is_degree: %i\n", storage->is_degree?1:0);

    if (storage->pkey) {
        fprintf(output, "pkey: ");
        buffer_flush(storage->pkey, output);
        fprintf(output, "\n");
    }

    fprintf(output, "pkey_column_number: %i\n", storage->pkey_column_number);

    if (storage->pkey_sequence) {
        fprintf(output, "pkey_sequence: ");
        buffer_flush(storage->pkey_sequence, output);
        fprintf(output, "\n");
    }

    if (storage->pkey_default) {
        fprintf(output, "pkey_default: ");
        buffer_flush(storage->pkey_default, output);
        fprintf(output, "\n");
    }

    if (storage->attributes) {
        fprintf(output, "attributes: ");
        array_flush(storage->attributes, output);
        fprintf(output, "\n");
    }

    if (storage->not_null_columns) {
        fprintf(output, "not_null_columns: ");
        list_flush(storage->not_null_columns, output);
        fprintf(output, "\n");
    }

}
#endif


/**
 * Used by --check command line option
 */
void ows_layers_storage_flush(ows * o, FILE * output)
{
    ows_layer_node *ln;

    assert(o);
    assert(o->layers);

    for (ln = o->layers->first ; ln ; ln = ln->next) {
        if (ln->layer->storage) { /**\todo Print Layer info if storage NULL*/
            fprintf(output, " - %s.%s (%i) -> %s.%s [",
                ln->layer->storage->schema->buf,
                ln->layer->storage->table->buf,
                ln->layer->storage->srid,
                ln->layer->ns_prefix->buf,
                ln->layer->name->buf);

            if (ln->layer->retrievable) fprintf(output, "R");
            if (ln->layer->writable)    fprintf(output, "W");
            fprintf(output, "]\n");
#ifdef OWS_DEBUG
            ows_layer_flush(ln->layer, stdout);
#endif
        }
    }
}


/**
 * Fill all layers storage
 */
void ows_layers_storage_fill(ows * o)
{
    ows_layer_node *ln;
    ows_db * db;

    assert(o);
    assert(o->layers);

    for (ln = o->layers->first ; ln ; ln = ln->next) {
        db = NULL;

        if (ln->layer->storage)
            db = ows_db_get_by_name(o->dbs, ln->layer->storage->db_name);

        if (db && ln->layer->storage->schema && ln->layer->storage->table) {
            ln->layer->storage->db = db;

            if ( buffer_cmp(db->type, "pg"))
                ows_psql_layer_storage_fill(o, ln->layer);
            else if ( buffer_cmp(db->type, "mysql"))
                ows_mysql_layer_storage_fill(o, ln->layer);
        }
    }
}

/*
 * vim: expandtab sw=4 ts=4
 */
