/*
  Copyright (c) <2007-2012> <Barbara Philippot - Olivier Courtin>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include "ows.h"


/*
 * Connect the ows to the PG database
 */
void ows_db_conn_pg(ows * o, ows_db * db)
{
    buffer * db_con;

    assert(o);
    assert(db);

    db_con = buffer_init();

    /*
    db->pg = PQconnectdb(con_str);
    */

    buffer_add_str(db_con, "host = ");
    buffer_add_str(db_con, array_get(db->db_dsn, "host")->buf);
    buffer_add_str(db_con, " user = ");
    buffer_add_str(db_con, array_get(db->db_dsn, "user")->buf);
    buffer_add_str(db_con, " password = ");
    buffer_add_str(db_con, array_get(db->db_dsn, "password")->buf);
    buffer_add_str(db_con, " dbname = ");
    buffer_add_str(db_con, array_get(db->db_dsn, "dbname")->buf);
    buffer_add_str(db_con, " port = ");
    buffer_add_str(db_con, array_get(db->db_dsn, "port")->buf);

    db->pg = PQconnectdb(db_con->buf);

    buffer_free(db_con);

    if (PQstatus(db->pg) != CONNECTION_OK) {
        buffer_add_str(db->type, "\n    NO conn - ");
        buffer_add_str(db->type, PQerrorMessage(db->pg));
    }

    if (PQsetClientEncoding(db->pg, db->db_encoding->buf)) {
        buffer_add_str(db->type, "\n    Wrong db encoding - ");
        buffer_add_str(db->type, PQerrorMessage(db->pg));
    }

/** 
\todo add postgis version
\code
    o->postgis_version = ows_psql_postgis_version(o);
    if (!o->postgis_version)
        ows_error(o, OWS_ERROR_CONNECTION_FAILED, "No PostGIS available in database", "init_OWS");
    else if (ows_version_get(o->postgis_version) < 150)
        ows_error(o, OWS_ERROR_CONNECTION_FAILED, "PostGIS version must be at least 1.5.0", "init_OWS");
\endcode
*/
}


/*
 * Execute an SQL request 
 */
PGresult * ows_psql_exec(ows *o, const char *sql)
{
    assert(o);
    assert(sql);
    assert(o->pg);
     
    ows_log(o, 8, sql);
    return PQexecParams(o->pg, sql, 0, NULL, NULL, NULL, NULL, 0); 
}


/*
 * Execute an SQL request 
 */
PGresult * ows_psql_exe(ows * o, ows_db * db, const char * sql)
{
    assert(o);
    assert(sql);
    assert(db->pg);
     
    ows_log(o, 8, sql);
    return PQexecParams(db->pg, sql, 0, NULL, NULL, NULL, NULL, 0); 
}


/*
 * Check if a given WKT geometry is or not valid
 */
/**
 * \todo rewrite without the use PostGIS
 */
bool ows_psql_is_geometry_valid(ows * o, buffer * geom)
{
    buffer *sql;
    PGresult *res;
    bool ret = false;

    assert(o);
    assert(geom);

    sql = buffer_init();
    buffer_add_str(sql, "SELECT ST_isvalid(ST_geometryfromtext('");
    buffer_copy(sql, geom);
    buffer_add_str(sql, "', -1));");
    
    res = ows_psql_exec(o, sql->buf);
    buffer_free(sql);

    if (PQresultStatus(res) == PGRES_TUPLES_OK && PQntuples(res) == 1
            && (char) PQgetvalue(res, 0, 0)[0] ==  't') 
        ret = true;

    PQclear(res);
    return ret;
}


/*
 * Return the column's name matching the specified number from table
 * (Only use in specific FE position function, so not directly inside
 * storage handle mechanism)
 */
/**
 * \todo to psql_storage
 */
buffer * ows_psql_column_name(ows * o, buffer * layer_name, int number)
{
    buffer *sql;
    PGresult *res;
    buffer *column;

    assert(o);
    assert(layer_name);

    sql = buffer_init();
    column = buffer_init();

    buffer_add_str(sql, "SELECT a.attname FROM pg_class c, pg_attribute a, pg_type t WHERE c.relname ='");
    buffer_copy(sql, layer_name); /** \todo must be table name, and position in table <> position in layer,\n and remember: includ/exclud_items & add/drop clumn */
    buffer_add_str(sql, "' AND a.attnum > 0 AND a.attrelid = c.oid AND a.atttypid = t.oid AND a.attnum = ");
    buffer_add_int(sql, number);

    res = ows_psql_exec(o, sql->buf);
    buffer_free(sql);

    if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) != 1) {
        PQclear(res);
        return column;
    }

    buffer_add_str(column, PQgetvalue(res, 0, 0));
    PQclear(res);

    return column;
}


/* 
 * Retrieve an ows_version, related to current PostGIS version.
 */
ows_version * ows_psql_postgis_version(ows *o)
{
    list *l;
    PGresult * res;
    ows_version * v = NULL; 

    res = ows_psql_exec(o, "SELECT substr(postgis_full_version(), 10, 5)");
    if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) != 1) {
        PQclear(res);
        return NULL;
    }

    l = list_explode_str('.', (char *) PQgetvalue(res, 0, 0));

    if (    l->size == 3 
             && check_regexp(l->first->value->buf,       "^[0-9]+$")
             && check_regexp(l->first->next->value->buf, "^[0-9]+$")
             && check_regexp(l->last->value->buf,        "^[0-9]+$") )
    {
        v = ows_version_init();
        v->major   = atoi(l->first->value->buf);
        v->minor   = atoi(l->first->next->value->buf);
        v->release = atoi(l->last->value->buf);
    }
 
    list_free(l);
    PQclear(res);
    return v;
}


/*
 * Convert a PostgreSql type to a valid
 * OGC XMLSchema's type
 */
char *ows_psql_to_xsd(buffer * type, ows_version *version)
{
    int wfs_version;

    assert(type);
    assert(version);

    wfs_version = ows_version_get(version); 
 
    if (buffer_case_cmp(type, "geometry")) return "gml:GeometryPropertyType";
    if (buffer_cmp(type, "geography")) return "gml:GeometryPropertyType";
    if (buffer_cmp(type, "int2")) return "short";
    if (buffer_cmp(type, "int4")) return "int";
    if (buffer_cmp(type, "int8")) return "long";
    if (buffer_cmp(type, "float4")) return "float";
    if (buffer_cmp(type, "float8")) return "double";
    if (buffer_cmp(type, "bool")) return "boolean";
    if (buffer_cmp(type, "bytea")) return "byte";
    if (buffer_cmp(type, "date")) return "date";
    if (buffer_cmp(type, "time")) return "time";
    if (buffer_ncmp(type, "numeric", 7)) return "decimal";
    if (buffer_ncmp(type, "timestamp", 9)) return "dateTime"; /* Could be also timestamptz */
    if (buffer_cmp(type, "POINT")) return "gml:PointPropertyType";
    if (buffer_cmp(type, "LINESTRING") && wfs_version == 100) return "gml:LineStringPropertyType";
    if (buffer_cmp(type, "LINESTRING") && wfs_version == 110) return "gml:CurvePropertyType";
    if (buffer_cmp(type, "POLYGON") && wfs_version == 100) return "gml:PolygonPropertyType";
    if (buffer_cmp(type, "POLYGON") && wfs_version == 110) return "gml:SurfacePropertyType";
    if (buffer_cmp(type, "MULTIPOINT")) return "gml:MultiPointPropertyType";
    if (buffer_cmp(type, "MULTILINESTRING") && wfs_version == 100) return "gml:MultiLineStringPropertyType";
    if (buffer_cmp(type, "MULTILINESTRING") && wfs_version == 110) return "gml:MultiCurvePropertyType";
    if (buffer_cmp(type, "MULTIPOLYGON") && wfs_version == 100) return "gml:MultiPolygonPropertyType";
    if (buffer_cmp(type, "MULTIPOLYGON") && wfs_version == 110) return "gml:MultiSurfacePropertyType";
    if (buffer_cmp(type, "GEOMETRYCOLLECTION")) return "gml:MultiGeometryPropertyType";

    return "string";
}


/*
 * Convert a date from PostgreSQL to XML
 */
buffer *ows_psql_timestamp_to_xml_time(char *timestamp)
{
    buffer *time;

    assert(timestamp);

    time = buffer_init();
    buffer_add_str(time, timestamp);
    if (!time->use) return time;

    buffer_replace(time, " ", "T");

    if (check_regexp(time->buf, "\\+"))
        buffer_add_str(time, ":00");
    else
        buffer_add_str(time, "Z");

    return time;
}


/*
 * Generate a new buffer id supposed to be unique for a given layer name
 */
buffer *ows_psql_generate_id(ows * o, buffer * layer_name)
{
    ows_layer_node *ln;
    buffer * id, *sql_id;
    FILE *fp;
    PGresult * res;
    int i, seed_len;
    char * seed = NULL;

    assert(o);
    assert(o->layers);
    assert(layer_name);

    /* Retrieve layer node pointer */
    for (ln = o->layers->first ; ln ; ln = ln->next) {
        if (ln->layer->name && ln->layer->storage
            && !strcmp(ln->layer->name->buf, layer_name->buf)) break;
    }
    assert(ln);

    id = buffer_init();

   /* If PK have a sequence in PostgreSQL database,
    * retrieve next available sequence value
    */
    if (ln->layer->storage->pkey_sequence) {
        sql_id = buffer_init();
        buffer_add_str(sql_id, "SELECT nextval('");
        buffer_copy(sql_id, ln->layer->storage->pkey_sequence);
        buffer_add_str(sql_id, "');");
        res = ows_psql_exec(o, sql_id->buf);
        buffer_free(sql_id);

        if (PQresultStatus(res) == PGRES_TUPLES_OK && PQntuples(res) == 1) {
            buffer_add_str(id, (char *) PQgetvalue(res, 0, 0));
            PQclear(res);
            return id;
        }

        /* FIXME: Shouldn't we return an error there instead ? */
        PQclear(res);
    } 
    
   /* If PK have a DEFAULT in PostgreSQL database,
    * retrieve next available DEFAULT value
    */
    if (ln->layer->storage->pkey_default) {
        sql_id = buffer_init();
        buffer_add_str(sql_id, "SELECT ");
        buffer_copy(sql_id, ln->layer->storage->pkey_default);
        buffer_add_str(sql_id, ";");
        res = ows_psql_exec(o, sql_id->buf);
        buffer_free(sql_id);

        if (PQresultStatus(res) == PGRES_TUPLES_OK && PQntuples(res) == 1) {
            buffer_add_str(id, (char *) PQgetvalue(res, 0, 0));
            PQclear(res);
            return id;
        }

        /* FIXME: Shouldn't we return an error there instead ? */
        PQclear(res);
     } 
 
   /*
    * If we don't have a PostgreSQL Sequence, we will try to 
    * generate a pseudo random keystring using /dev/urandom
    * Will so work only on somes/commons Unix system
    */
    seed_len = 6;
    seed = malloc(sizeof(char) * (seed_len * 3 + 1));  /* multiply by 3 to be able to deal
                                                       with hex2dec conversion */ 
    assert(seed);
    seed[0] = '\0';

    fp = fopen("/dev/urandom","r");
    if (fp) {
        for (i=0 ; i<seed_len ; i++)
            sprintf(seed,"%s%03d", seed, fgetc(fp));
        fclose(fp);
        buffer_add_str(id, seed);
        free(seed);

        return id;
    } 
    free(seed);

   /** Case where we not using PostgreSQL sequence, 
    * and OS don't have a /dev/urandom support
    * This case don't prevent to produce ID collision
    * Don't use it unless really no others choices !!!
    */
    srand((int) (time(NULL) ^ rand() % 1000) + 42); 
    srand((rand() % 1000 ^ rand() % 1000) + 42); 
    buffer_add_int(id, rand());

     return id;
}


/*
 * Return the number of rows returned by the specified requests
 */
int ows_psql_number_features(ows * o, list * from, list * where)
{
    buffer *sql;
    PGresult *res;
    list_node *ln_from, *ln_where;
    int nb;

    assert(o);
    assert(from);
    assert(where);

    nb = 0;

    /* checks if from list and where list have the same size */
    if (from->size != where->size) return nb;


    for (ln_from = from->first, ln_where = where->first; 
	 ln_from;
         ln_from = ln_from->next, ln_where = ln_where->next) {
             sql = buffer_init();

             /* execute the request */
             buffer_add_str(sql, "SELECT count(*) FROM \"");
             buffer_copy(sql, ln_from->value);
             buffer_add_str(sql, "\" ");
             buffer_copy(sql, ln_where->value);
             res = ows_psql_exec(o, sql->buf);
             buffer_free(sql);

             if (PQresultStatus(res) != PGRES_TUPLES_OK) {
                 PQclear(res);
                 return -1;
             }
             nb = nb + atoi(PQgetvalue(res, 0, 0));
             PQclear(res);
     }

    return nb;
}


xmlNodePtr ows_psql_recursive_parse_gml(ows * o, xmlNodePtr n, xmlNodePtr result)
{
    xmlNodePtr c;

    assert(o);
    assert(n);

    if (result) return result;  /* avoid recursive loop */

    /* We are looking for the geometry part inside GML doc */
    for (; n ; n = n->next) {

        if (n->type != XML_ELEMENT_NODE) continue;

        /* Check on namespace GML 3 and GML 3.2 */
	if (    strcmp("http://www.opengis.net/gml",     (char *) n->ns->href)
             && strcmp("http://www.opengis.net/gml/3.2", (char *) n->ns->href)) continue;

        /* GML SF Geometries Types */
        if (   !strcmp((char *) n->name, "Point")
            || !strcmp((char *) n->name, "LineString")
            || !strcmp((char *) n->name, "LinearRing")
            || !strcmp((char *) n->name, "Curve")
            || !strcmp((char *) n->name, "Polygon")
            || !strcmp((char *) n->name, "Surface")
            || !strcmp((char *) n->name, "MultiPoint")
            || !strcmp((char *) n->name, "MultiLineString")
            || !strcmp((char *) n->name, "MultiCurve")
            || !strcmp((char *) n->name, "MultiPolygon")
            || !strcmp((char *) n->name, "MultiSurface")
            || !strcmp((char *) n->name, "MultiGeometry")) return n;

        /* Recursive exploration */
        if (n->children)
            for (c = n->children ; c ; c = c->next)
                if ((result = ows_psql_recursive_parse_gml(o, c, NULL)))
                    return result;
    }

    return NULL;
}


/*
 * Transform a GML geometry to PostGIS EWKT
 * Return NULL on error
 */
/**
 * \todo rewrite without the use PostGIS
 */
buffer * ows_psql_gml_to_sql(ows * o, xmlNodePtr n, int srid)
{
    PGresult *res;
    xmlNodePtr g;
    buffer *result, *sql, *gml;

    assert(o);
    assert(n);

    g = ows_psql_recursive_parse_gml(o, n, NULL);
    if (!g) return NULL;    /* No Geometry founded in GML doc */

    /* Retrieve the sub doc and launch GML parse via PostGIS */
    gml = buffer_init();
    cgi_add_xml_into_buffer(gml, g);
    
    sql = buffer_init();
    buffer_add_str(sql, "SELECT ST_GeomFromGML('");
    buffer_add_str(sql, gml->buf);

    if (ows_version_get(o->postgis_version) >= 200) {
       buffer_add_str(sql, "',");
       buffer_add_int(sql, srid);
       buffer_add_str(sql, ")");
    } else { 
       /* Means PostGIS 1.5 */
       buffer_add_str(sql, "')");
    }

    res = ows_psql_exec(o, sql->buf);
    buffer_free(gml);

    /* GML Parse errors cases */
    if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) != 1) {
        buffer_free(sql);
        PQclear(res);
        return NULL;
    }

    result = buffer_init();
    buffer_add_str(result, PQgetvalue(res, 0, 0));
    PQclear(res);

    /* Check if geometry is valid */
    if (o->check_valid_geom) {

        buffer_empty(sql);
        buffer_add_str(sql, "SELECT ST_IsValid('");
        buffer_add_str(sql, result->buf);
        buffer_add_str(sql, "')");

        res = ows_psql_exec(o, sql->buf);

        if (    PQresultStatus(res) != PGRES_TUPLES_OK
             || PQntuples(res) != 1
             || (char) PQgetvalue(res, 0, 0)[0] !=  't') {
            buffer_free(sql);
            buffer_free(result);
            PQclear(res);
            return NULL;
        }
        PQclear(res);
    }

    buffer_free(sql);
 
    return result;
}


/* 
 * Use PostgreSQL native handling to escape string
 * string returned must be freed by the caller
 * return NULL on error
 */
char *ows_psql_escape_string(ows *o, const char *content)
{
    char *content_escaped;
    int error = 0;

    assert(o);
    assert(o->pg);
    assert(content);

    content_escaped = malloc(strlen(content) * 2 + 1);
    PQescapeStringConn(o->pg, content_escaped, content, strlen(content), &error);
    
    if (error != 0) return NULL;

    return content_escaped;
}


/*
 * Return SRID from a given geometry
 */
 /**
 * \todo rewrite without the use PostGIS
 */
int ows_psql_geometry_srid(ows *o, const char *geom)
{
    int srid;
    buffer *sql;
    PGresult *res;
    
    assert(o);
    assert(o->pg);
    assert(geom);

    sql = buffer_from_str("SELECT ST_SRID('");
    buffer_add_str(sql, geom);
    buffer_add_str(sql, "'::geometry)");

    res = ows_psql_exec(o, sql->buf);

    if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) != 1) srid = -1;
    else srid = atoi((char *) PQgetvalue(res, 0, 0));

    buffer_free(sql);
    PQclear(res);

    return srid;
}


/*
 * Fill layer storage from given db
 */
void ows_psql_layer_storage_fill(ows * o, ows_layer * l)
{
    buffer * sql;
    PGresult *res;
    int i, end;

    assert(o);
    assert(l);
    assert(l->storage);

    sql = buffer_init();

    buffer_add_str(sql, "SELECT srid, geo_col FROM (");
    buffer_add_str(sql, "  SELECT f_table_schema, f_table_name, f_geometry_column as geo_col, srid, type FROM geometry_columns");
    buffer_add_str(sql, "    union all");
    buffer_add_str(sql, "  SELECT f_table_schema, f_table_name, f_geography_column as geo_col, srid, type FROM geography_columns");
    buffer_add_str(sql, ") as tu");

    buffer_add_str(sql, " WHERE tu.f_table_schema='");
    buffer_copy(sql, l->storage->schema);
    buffer_add_str(sql, "' AND tu.f_table_name='");
    buffer_copy(sql, l->storage->table);
    buffer_add_str(sql, "'");

    res = ows_psql_exe(o, l->storage->db, sql->buf);
    buffer_empty(sql);

    if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) == 0) {
        PQclear(res);

/**
 * \todo Drop storage - NOT GOOD. I want used pseudo layer without geom/geog columns.
 */
        if (l->storage)
            ows_layer_storage_free(l->storage);
        l->storage = NULL;

/** \todo Wrong place for error check
\code
        ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED,
           "All config file layers are not availables in geometry_columns or geography_columns",
           "storage");
\endcode
*/
        buffer_free(sql);
        return;
    }

    l->storage->srid = atoi(PQgetvalue(res, 0, 0));

    for (i = 0, end = PQntuples(res); i < end; i++)
        list_add_str(l->storage->geom_columns, PQgetvalue(res, i, 1));

    buffer_add_str(sql, "SELECT * FROM spatial_ref_sys WHERE srid=");
    buffer_add_str(sql, PQgetvalue(res, 0, 0));
    buffer_add_str(sql, " AND proj4text like '%%units=m%%'");

    PQclear(res);

    res = ows_psql_exe(o, l->storage->db, sql->buf);
    buffer_free(sql);

    if (PQntuples(res) != 1)
        l->storage->is_degree = true;
    else
        l->storage->is_degree = false;

    PQclear(res);

    ows_psql_storage_fill_attributes(o, l);
    ows_psql_storage_fill_not_null(o, l);
    ows_psql_storage_fill_pkey(o, l);
    ows_psql_storage_fill_char_max_length(o, l);
    ows_psql_storage_fill_constraints(o, l);
}


/*
 * Retrieve columns name and type of a table related a given layer
 */
void ows_psql_storage_fill_attributes(ows * o, ows_layer * l)
{
    buffer *sql;
    PGresult *res;
    buffer *b, *t;
    int i, end;
    list_node *ln;

    assert(o);
    assert(l);
    assert(l->storage);

    sql = buffer_init();

    buffer_add_str(sql, "SELECT a.attname AS field, t.typname AS type ");
    buffer_add_str(sql, "FROM pg_class c, pg_attribute a, pg_type t, pg_namespace n WHERE n.nspname = '");
    buffer_copy(sql, l->storage->schema);
    buffer_add_str(sql, "' AND c.relname = '");
    buffer_copy(sql, l->storage->table);
    buffer_add_str(sql, "' AND c.relnamespace = n.oid AND a.attrelid = c.oid AND a.atttypid = t.oid");

/**\todo Add function that return string like "IN ('comma', 'separated', 'list')", for code:
 * \code
 */
    if (l->exclude_items) {
        buffer_add_str(sql, " AND a.attname NOT IN (");
        for (ln = l->exclude_items->first ; ln ; ln = ln->next) {
            buffer_add_str(sql, "'");
            buffer_copy(sql, ln->value);
            buffer_add_str(sql, "', ");
        }
        buffer_add_str(sql, " '')");
    }
    if (l->include_items) {
        buffer_add_str(sql, " AND a.attname IN (");
        for (ln = l->include_items->first ; ln ; ln = ln->next) {
            buffer_add_str(sql, "'");
            buffer_copy(sql, ln->value);
            buffer_add_str(sql, "', ");
        }
        buffer_add_str(sql, " '');");
    } else { 
        buffer_add_str(sql, " AND a.attnum > 0;");
    }
/**\endcode*/
    res = ows_psql_exe(o, l->storage->db, sql->buf);
    buffer_free(sql);

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED, "Unable to access pg_* tables.", "fill_attributes");
        return;
    }

    for (i = 0, end = PQntuples(res); i < end; i++) {
        b = buffer_init();
        t = buffer_init();
        buffer_add_str(b, PQgetvalue(res, i, 0));
        buffer_add_str(t, PQgetvalue(res, i, 1));

        /**\todo If the column is a geometry, get its real geometry type. What about geography? */
        if (buffer_cmp(t, "geometry")){
            PGresult *geom_res;
            buffer *geom_sql = buffer_init();
            buffer_add_str(geom_sql, "SELECT type from geometry_columns where f_table_schema='");
            buffer_copy(geom_sql, l->storage->schema);
            buffer_add_str(geom_sql,"' and f_table_name='");
            buffer_copy(geom_sql, l->storage->table);
            buffer_add_str(geom_sql,"' and f_geometry_column='");
            buffer_copy(geom_sql, b);
            buffer_add_str(geom_sql,"';");

            geom_res = ows_psql_exe(o, l->storage->db, geom_sql->buf);
            buffer_free(geom_sql);

            if (PQresultStatus(geom_res) != PGRES_TUPLES_OK || PQntuples(geom_res) == 0) {
                PQclear(res);
                PQclear(geom_res);
                ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED,
                    "Unable to access geometry_columns table, try Populate_Geometry_Columns()", "fill_attributes");
                return;
            }

            buffer_empty(t);
            buffer_add_str(t, PQgetvalue(geom_res, 0, 0));
            PQclear(geom_res);
        }

        array_add(l->storage->attributes, b, t);
    }
    PQclear(res);
}


/*
 * Retrieve not_null columns of a table related a given layer
 */
void ows_psql_storage_fill_not_null(ows * o, ows_layer * l)
{
    int i, nb_result;
    buffer *sql, *b;
    PGresult *res;

    assert(o);
    assert(l);
    assert(l->storage);

    sql = buffer_init();
    buffer_add_str(sql, "SELECT a.attname AS field FROM pg_class c, pg_attribute a, pg_type t, pg_namespace n ");
    buffer_add_str(sql, "WHERE n.nspname = '");
    buffer_copy(sql, l->storage->schema);
    buffer_add_str(sql, "' AND c.relname = '");
    buffer_copy(sql, l->storage->table);
    buffer_add_str(sql, "' AND c.relnamespace = n.oid AND a.attnum > 0 AND a.attrelid = c.oid ");
    buffer_add_str(sql, "AND a.atttypid = t.oid AND a.attnotnull = 't' AND a.atthasdef='f'");

    res = ows_psql_exe(o, l->storage->db, sql->buf);
    buffer_free(sql);

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED, "Unable to access pg_* tables.", "not_null columns");
        return;
    }

    nb_result = PQntuples(res);
    if (nb_result) l->storage->not_null_columns = list_init();
    for (i = 0 ; i < nb_result ; i++) {
        b = buffer_init();
        buffer_add_str(b, PQgetvalue(res, i, 0));
        list_add(l->storage->not_null_columns, b);
    }

    PQclear(res);
}


/*
 * Retrieve pkey column of a table related a given layer
 * And if success try also to retrieve a related pkey sequence
 */
void ows_psql_storage_fill_pkey(ows * o, ows_layer * l)
{
    buffer *sql;
    PGresult *res;

    assert(o);
    assert(l);
    assert(l->storage);

    sql = buffer_init();

    buffer_add_str(sql, "SELECT c.column_name FROM information_schema.constraint_column_usage c, pg_namespace n ");
    buffer_add_str(sql, "WHERE n.nspname = '");
    buffer_copy(sql, l->storage->schema);
    buffer_add_str(sql, "' AND c.table_name = '");
    buffer_copy(sql, l->storage->table);
    buffer_add_str(sql, "' AND c.constraint_name = (");

    buffer_add_str(sql, "SELECT c.conname FROM pg_class r, pg_constraint c, pg_namespace n ");
    buffer_add_str(sql, "WHERE r.oid = c.conrelid AND relname = '");
    buffer_copy(sql, l->storage->table);
    buffer_add_str(sql, "' AND r.relnamespace = n.oid AND n.nspname = '");
    buffer_copy(sql, l->storage->schema);
    buffer_add_str(sql, "' AND c.contype = 'p')");

    res = ows_psql_exe(o, l->storage->db, sql->buf);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        buffer_free(sql);
        ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED, "Unable to access pg_* tables.", "pkey column");
    return;
    }

    /* Layer could have no Pkey indeed... (An SQL view for example) */
    if (l->pkey || PQntuples(res) == 1) {
        l->storage->pkey = buffer_init();
        if (l->pkey) {
            /**\todo check the column (l->pkey) in the table */
            buffer_copy(l->storage->pkey, l->pkey);
        } 
        else {
            buffer_add_str(l->storage->pkey, PQgetvalue(res, 0, 0));
        }
        buffer_empty(sql);
        PQclear(res);

        /* Retrieve the Pkey column number */
        buffer_add_str(sql, "SELECT a.attnum FROM pg_class c, pg_attribute a, pg_type t, pg_namespace n");
        buffer_add_str(sql, " WHERE a.attrelid = c.oid AND a.atttypid = t.oid AND n.nspname='");
        buffer_copy(sql, l->storage->schema);
        buffer_add_str(sql, "' AND c.relname='");
        buffer_copy(sql, l->storage->table);
        buffer_add_str(sql, "' AND a.attname='");
        buffer_copy(sql, l->storage->pkey);
        buffer_add_str(sql, "'");
        res = ows_psql_exe(o, l->storage->db, sql->buf);
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            PQclear(res);
            buffer_free(sql);
            ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED, "Unable to find pkey column number.", "pkey_column number");
        return;
        }

        /* -1 because column number start at 1 */
        l->storage->pkey_column_number = atoi(PQgetvalue(res, 0, 0)) - 1;
        buffer_empty(sql);
        PQclear(res);

        /* Now try to find a sequence related to this Pkey */
        buffer_add_str(sql, "SELECT pg_get_serial_sequence('");
        buffer_copy(sql, l->storage->schema);
        buffer_add_str(sql, ".\"");
        buffer_copy(sql, l->storage->table);
        buffer_add_str(sql, "\"', '");
        buffer_copy(sql, l->storage->pkey);
        buffer_add_str(sql, "');");

        res = ows_psql_exe(o, l->storage->db, sql->buf);
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            PQclear(res);
            buffer_free(sql);
            ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED,
                  "Unable to use pg_get_serial_sequence.", "pkey_sequence retrieve");
            return;
        }
        
        /* Even if no sequence found, this function return an empty row
         * so we must check that result string returned > 0 char
         */
        if (PQntuples(res) == 1 && strlen((char *) PQgetvalue(res, 0, 0)) > 0) {
            l->storage->pkey_sequence = buffer_init();
            buffer_add_str(l->storage->pkey_sequence, PQgetvalue(res, 0, 0));
        }

        buffer_empty(sql);
        PQclear(res);
        /* Now try to find a DEFAULT value related to this Pkey */
        buffer_add_str(sql, "SELECT column_default FROM information_schema.columns WHERE table_schema = '");
        buffer_copy(sql, l->storage->schema);
        buffer_add_str(sql, "' AND table_name = '");
        buffer_copy(sql, l->storage->table);
        buffer_add_str(sql, "' AND column_name = '");
        buffer_copy(sql, l->storage->pkey);
        buffer_add_str(sql, "' AND table_catalog = current_database();");

        res = ows_psql_exe(o, l->storage->db, sql->buf);
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            PQclear(res);
            buffer_free(sql);
            ows_error(o, OWS_ERROR_REQUEST_SQL_FAILED,
                "Unable to SELECT column_default FROM information_schema.columns.", 
                "pkey_default retrieve");
            return;
        }

        /* Even if no DEFAULT value found, this function return an empty row
         * so we must check that result string returned > 0 char
         */
        if (PQntuples(res) == 1 && strlen((char *) PQgetvalue(res, 0, 0)) > 0) {
            l->storage->pkey_default = buffer_init();
            buffer_add_str(l->storage->pkey_default, PQgetvalue(res, 0, 0));
        }
    }

    PQclear(res);
    buffer_free(sql);
}


/*
 * Retrieve columns character_maximum_length of a table related a given layer,
 * and fill layer->storage->char_max_length
 */
void ows_psql_storage_fill_char_max_length(ows * o, ows_layer * l)
{
    buffer * sql;
    PGresult * res;
    buffer * column_name;
    buffer * char_max_length;
    array_node * an;
    
    assert(o);
    assert(l);

    sql = buffer_init();
    
    for (an = l->storage->attributes->first ; an ; an = an->next){
        buffer_add_str(sql, "SELECT character_maximum_length FROM information_schema.columns WHERE table_schema = '");
        buffer_add_str(sql, l->storage->schema->buf);
        buffer_add_str(sql, "' and table_name = '");
        buffer_add_str(sql, l->storage->table->buf);
        buffer_add_str(sql, "' and column_name = '");
        buffer_add_str(sql, an->key->buf);
        buffer_add_str(sql, "';");
               
        res = ows_psql_exe(o, l->storage->db, sql->buf);
        buffer_empty(sql);

        if ((PQresultStatus(res) == PGRES_TUPLES_OK) && (PQntuples(res) == 1) && (strcmp(PQgetvalue(res, 0, 0), ""))){
            column_name = buffer_init();
            char_max_length = buffer_init();
            buffer_add_str(column_name, an->key->buf);
            buffer_add_str(char_max_length, PQgetvalue(res, 0, 0));
            array_add(l->storage->char_max_length, column_name, char_max_length);
        }
        PQclear(res);
    }
    buffer_free(sql);
}


/*
 * Fill the list of possible values for a column according to the constraint value.
 * (layer->storage->constraints)
 * Used to return enumeration constraints in describe feature type request.
 */
void ows_psql_storage_fill_constraints(ows * o, ows_layer * l)
{
    array_node * an;
    buffer * buf;
    buffer * constraint_name;
    buffer * constraint_value;
    buffer * sql;
    PGresult * res;
    size_t i;
    size_t j;

    assert(o);
    assert(l);

    sql = buffer_init();
    constraint_name = buffer_init();
    constraint_value = buffer_init();

    for (an = l->storage->attributes->first ; an ; an = an->next){
        buffer_add_str(sql, "SELECT constraint_name FROM information_schema.constraint_column_usage WHERE table_schema = '");
        buffer_add_str(sql, l->storage->schema->buf);
        buffer_add_str(sql, "' and table_name = '");
        buffer_add_str(sql, l->storage->table->buf);
        buffer_add_str(sql, "' and column_name = '");
        buffer_add_str(sql, an->key->buf);
        buffer_add_str(sql, "';");

        res = ows_psql_exe(o, l->storage->db, sql->buf);
        buffer_empty(sql);
        buffer_empty(constraint_name);

        if ((PQresultStatus(res) == PGRES_TUPLES_OK) && (PQntuples(res) == 1) && (strcmp(PQgetvalue(res, 0, 0), ""))){
            buffer_add_str(constraint_name, PQgetvalue(res, 0, 0));
            PQclear(res);

            buffer_add_str(sql, "SELECT check_clause FROM information_schema.check_constraints WHERE constraint_name = '");
            buffer_add_str(sql, constraint_name->buf);
            buffer_add_str(sql, "';");

            res = ows_psql_exe(o, l->storage->db, sql->buf);
            buffer_empty(sql);
            buffer_empty(constraint_value);

            if ((PQresultStatus(res) == PGRES_TUPLES_OK) && (PQntuples(res) == 1) && (strcmp(PQgetvalue(res, 0, 0), ""))){

                buffer_add_str(constraint_value, PQgetvalue(res, 0, 0));
                PQclear(res);

                j=0;
                buf = buffer_init();
                for (i = 0; constraint_value->buf[i] != '\0'; i++){
                    if(constraint_value->buf[i] == '\''){
                        j++;
                        if(j%2==1)
                            buf = buffer_init();
                        else
                            alist_add(l->storage->constraints, an->key, buf);
                    }
                    else
                        if(j%2==1)
                            buffer_add(buf, constraint_value->buf[i]);
                }
            } else
                PQclear(res);
        } else
            PQclear(res);
    }
    buffer_free(sql);
    buffer_free(constraint_name);
    buffer_free(constraint_value);
}
