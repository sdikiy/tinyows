#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <string.h>

#include "ows.h"
/* #include "ows_db.h" */


/*
 * Initialize a layer
 */
ows_db *ows_db_init()
{
    ows_db *db = malloc(sizeof(ows_db));
    assert(db);

    db->name = buffer_init();
    db->type = buffer_init();
    db->pg = NULL;
    db->mysql = NULL;
    db->db_dsn = array_init();
    db->db_encoding = buffer_init();

    return db;
}


/*
 * Free a layer
 */
void ows_db_free(ows_db * db)
{
    assert(db);

    if (db->name)           buffer_free(db->name);
    if (db->type)           buffer_free(db->type);
    if (db->pg)             PQfinish(db->pg);
    if (db->mysql)          mysql_close(db->mysql);
    if (db->db_dsn)         array_free(db->db_dsn);
    if (db->db_encoding)    buffer_free(db->db_encoding);

    free(db);
    db = NULL;
}


/*
 * Initialize a db node
 */
ows_db_node *ows_db_node_init()
{
    ows_db_node *ln = malloc(sizeof(ows_db_node));
    assert(ln);

    ln->prev = NULL;
    ln->next = NULL;
    ln->db = NULL;

    return ln;
}


/*
 * Free a db node
 */
void ows_db_node_free(ows_db_list * ll, ows_db_node * ln)
{
    assert(ln);

    if (ln->prev) ln->prev = NULL;
    if (ln->next)
    {
        if (ll) ll->first = ln->next;
        ln->next = NULL;
    } else if (ll) ll->first = NULL;

    if (ln->db) ows_db_free(ln->db);

    free(ln);
    ln = NULL;
}


/*
 * Initialize an ows db structure
 */
ows_db_list *ows_db_list_init()
{
    ows_db_list *db_list;

    db_list = malloc(sizeof(ows_db_list));
    assert(db_list);

    db_list->first = NULL;
    db_list->last = NULL;
    return db_list;
}


/*
 * Free an ows db structure
 */
void ows_db_list_free(ows_db_list * db_list)
{
    assert(db_list);

    while (db_list->first) ows_db_node_free(db_list, db_list->first);
    db_list->last = NULL;
    free(db_list);
    db_list = NULL;
}


/*
 * Add a db to a db's list
 */
void ows_db_list_add(ows_db_list * db_list, ows_db * db)
{
    ows_db_node *db_node = ows_db_node_init();

    assert(db_list);
    assert(db);

    db_node->db = db;
    if (!db_list->first) {
        db_node->prev = NULL;
        db_list->first = db_node;
    } else {
        db_node->prev = db_list->last;
        db_list->last->next = db_node;
    }
    db_list->last = db_node;
    db_list->last->next = NULL;
}


/*
 * Retrieve a db from a db list or NULL if not found
 */
ows_db * ows_db_get_by_name(const ows_db_list * db_list, const buffer * name)
{
    ows_db_node * db_node;

    assert(db_list);
    assert(name);

    for (db_node = db_list->first; db_node ; db_node = db_node->next)
        if (db_node->db->name && !strcmp(db_node->db->name->buf, name->buf))
            return db_node->db;

    return (ows_db *) NULL;
}


/*
 * Connect the ows to databases specified in configuration file
 */
void ows_db_conn(ows * o)
{
    ows_db_node * db_node = NULL;

    assert(o);

    for (db_node = o->dbs->first; db_node ; db_node = db_node->next) {
        if (!strcmp(db_node->db->type->buf, "pg"))
            ows_db_conn_pg(o, db_node->db);

        if (!strcmp(db_node->db->type->buf, "mysql"))
            ows_db_conn_mysql(o, db_node->db);

    }

    return;
}


#ifdef OWS_DEBUG
/*
 * Flush a db to a given file
 * (mainly to debug purpose)
 */
void ows_db_flush(ows_db * db, FILE * output)
{
    assert(db);
    assert(output);

    if(db->name)
        fprintf(output, "name: %s\n", db->name->buf);

    if(db->type)
        fprintf(output, "type: %s\n", db->type->buf);

    if(db->db_encoding){
        fprintf(output, "db_encoding: ");
        buffer_flush(db->db_encoding, output);
        fprintf(output, "\n");
    }

    if (db->db_dsn) {
        fprintf(output, "db_dsn:\n");
        array_flush(db->db_dsn, output);
        fprintf(output, "\n");
    }
}


/*
 * Flush a db's list to a given file
 * (mainly to debug purpose)
 */
void ows_db_list_flush(ows_db_list * db_list, FILE * output)
{
    ows_db_node *db_node = NULL;
    assert(db_list);
    assert(output);

    for (db_node = db_list->first; db_node ; db_node = db_node->next) {
        fprintf(output, "--------------------\n");
        ows_db_flush(db_node->db, output);
    }
}
#endif
